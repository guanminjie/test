class UsersController < ApplicationController
 #get users/new
  def new
     @user = User.new

  end

  #post users/create 
  def create
  	 @user = User.new user_params     
     #账号注册
     #回调 sign_up (account,phone,email,password ) 
    response = @user.sign_up 
    puts response
    if response["errorCode"] == 0
      redirect_to "/sessions/new", notice: "#{response["mes"]}"
    else
      redirect_to action: "new" ,notice: "#{response["mes"]}"
    end
          
  end

  private
  
   def user_params
     params.require(:user).permit(:account,:password,:phone,:email,:params_confirmation)
   end

end
