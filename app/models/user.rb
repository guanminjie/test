
class User
    include ActiveModel::Model
    extend ActiveModel::Callbacks
    include ActiveModel::SecurePassword
    has_secure_password
    # 属性验证
    validates :account,:email,:phone,:password_digest ,presence: {message: "不能为空"} 
    validates :account, length: { maximum: 10 }
    validates :email ,format:{with: /\A([^\s]+)((?:[-a-z0-9]\.)[a-z]{2,})\z/i,message: "请正确输入邮箱格式"}
    validates :phone,  numericality:{only_integer: true}, length: { is: 11, wrong_length: "请正确输入11位手机号码"}
    validates :password,length: { in: 6..20,too_long: "最长长度不能超过%{count}",too_short: "最小长度不少于%{count}"}
    attr_accessor :account, :email ,:phone ,:password_digest
    
    # 定义 回调方法
    define_model_callbacks  :sign_up
    define_model_callbacks  :log_in

    #  用户注册
    def sign_up 
    	#(account,phone,email,password)
      puts account,phone,email,password

      run_callbacks(:sign_up) do 

      	#调用api 
    	request = Typhoeus::Request.new(
        "http://192.168.8.108:3001/api/v1/users/create",
        method: :post,
        body: {
        	data:{
        		account: self.account,
        		password: self.password,
        		phone: self.phone,
        		email: self.email
        	}
        }
      )
      request.run
  	  response = request.response
  	  response_data=ActiveSupport::JSON.decode response.body
      end
    end
     
     # 用户登录
     def log_in (account,password)
      puts account,password
      run_callbacks(:log_in) do 
    	request = Typhoeus::Request.new(
        "http://192.168.8.108:3001/api/v1/users/login",
        method: :post,
        body: {
        	data:{
        		user_name: account,
        		password: password,
        		
        	}
        }
      )
      request.run
  	  response = request.response
  	  response_data=ActiveSupport::JSON.decode response.body
      end
    end
    

    def can_validate?
         true
     end
end