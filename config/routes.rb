Rails.application.routes.draw do
  get 'uploads/new'

  post 'uploads/create'

  get 'sessions/new'

 post 'sessions/create'

  get 'sessions/delete'

  get 'users/new'

  post 'users/create'

  get 'renders/index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
